# todo1_test

TODO1 test application

Esta es una aplicación de pruebas.

Para correrla deberá:

- Clonar el repositorio
- Entrar en la carpeta backend desde eclipse o IntelliJ y correr el proyecto
- Deberá entrar a la capeta Secutiry/Utils y descomentar la función de esa clase. 
 (Aclaración) Esta clase se usa para crear los Roles del proyecto
- Correr de nuevo la aplicación backend

Siguiente paso:
- Entrar a la carpeta frontend
- Ejecutar el comando npm install
- Ejecutar el comando ng serve


Listo: el proyecto está corriendo en: localhost:8080
