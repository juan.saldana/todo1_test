import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  
  isLogged = false;
  loginFail = false;
  loginUser: any = {};
  userName: String = '';
  password: String = '';
  roles: String[] = [];

  constructor(
    private tokenService: TokenService,
    private authService: AuthService,
    private router: Router
    ) { }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
      this.loginFail = false;
      this.roles = this.tokenService.getArrayAuthorities();
    }
  }

  onLogin(): void {
    this.loginUser = { userName: this.userName, password: this.password };
    this.authService.login(this.loginUser).subscribe(
      response => {
        this.isLogged = true;
        this.loginFail = false;
        this.tokenService.setToken(response.token);
        this.tokenService.setUserName(response.userName);
        this.tokenService.setArrayAuthorities(response.authorities);
        this.roles = response.authorities;
        this.router.navigate(['/products'])
      },
      () => {
        this.isLogged = false;
        this.loginFail = true;
        
        alert('Error haciendo login');
      }
    );
  }
}
