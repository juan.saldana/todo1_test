import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Product } from '../../interfaces/products';
import { ProductService } from '../../services/products.service';
import { TokenService } from 'src/app/services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  public products: Product[] = [];
  public editProduct: any;
  public detailProduct: any;
  public deleteProduct: any;
  isLogged = false;
  roles: String[] = [];
  authority: string = '';
  admin = false;

  constructor(
    private producService: ProductService,
    private tokenService: TokenService,
    private router: Router
    ){ }

  ngOnInit(): void {
    if (this.tokenService.getToken()) {
      this.isLogged = true;
      this.roles = this.tokenService.getArrayAuthorities();
      this.roles.forEach( role => {
        if (role === 'ADMIN') this.admin = true;
      });
    }else {
      this.isLogged = false;
    }

    this.getProducts();
  }

  public getProducts(): void {
    this.producService.getProducts().subscribe(
      (response: Product[]) => {
        this.products = response;
      }, (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddProducts(addForm: NgForm): void {
    document.getElementById('add-product-form')?.click();
    this.producService.addProducts(addForm.value).subscribe(
      (response: Product) => {
        addForm.reset();
        this.products.push(response);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateProduct(editForm: NgForm, product: Product): void {
    this.producService.updateProducts(product).subscribe(
      () => {
        editForm.reset();
        this.getProducts();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        editForm.reset();
      }
    );
  }

  public onDeleteProduct(ProductId: number): void {
    this.producService.deleteProducts(ProductId).subscribe(
      () => {
        this.getProducts();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchProducts(key: string): void {
    const results: Product[] = [];
    for (const product of this.products) {
      if (product.name.includes(key.toLowerCase())) {
        results.push(product);
      }
    }

    this.products = results;

    if (this.products.length === 0) {
      setTimeout(() =>{
        this.getProducts();
      }, 1000);
    }

    if (!key) {
      this.getProducts();
    }
  }

  public onOpenModal(product: any, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'detail') {
      button.setAttribute('data-target', '#detailProductModal');
      this.detailProduct = product;
    }
    if (mode === 'add') {
      button.setAttribute('data-target', '#addProductModal');
    }
    if (mode === 'edit') {
      this.editProduct = product;
      button.setAttribute('data-target', '#updateProductModal');
    }
    if (mode === 'delete') {
      this.deleteProduct = product;
      button.setAttribute('data-target', '#deleteProductModal');
    }
    container?.appendChild(button);
    button.click();
  }

  onLogOut(): void {
    this.tokenService.logOut();
    this.router.navigate(['/login']);
  }
}
