import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { Product } from '../interfaces/products';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })

export class ProductService {
    private apiUrl = environment.apiBaseUrl;
    constructor(private http: HttpClient) { }

    public getProducts(): Observable<Product[]> {
        return this.http.get<Product[]>(`${this.apiUrl}/products/all`);
    }

    public addProducts(product: Product): Observable<Product> {
        return this.http.post<Product>(`${this.apiUrl}/products/add`, product);
    }

    public updateProducts(product: Product): Observable<Product> {
        return this.http.put<Product>(`${this.apiUrl}/products/update`, product);
    }

    public deleteProducts(productId: number): Observable<void> {
        return this.http.delete<void>(`${this.apiUrl}/products/delete/${productId}`);
    }

}