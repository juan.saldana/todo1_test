import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';
import { Login } from '../interfaces/login';
import { Jwt } from '../interfaces/jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authURL = 'http://localhost:8080/auth/'

  constructor(private httpClient: HttpClient) { }

  public newUser(user: User): Observable<any> {
    return this.httpClient.post<any>(this.authURL + 'new', user);
  }

  public login(credentials: Login): Observable<Jwt> {
    return this.httpClient.post<any>(this.authURL + 'login', credentials);
  }


}
