export interface User {
    id: number;
    name: String;
    lastName: String;
    userName: String;
    email: String;
    password: String,
    phone: number;
    imageUrl: String,
    userCode: String;
    authorities: String[];
}