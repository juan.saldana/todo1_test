package jsaldana.tech.backend.security.resource;

import jsaldana.tech.backend.security.RoleEnum;
import jsaldana.tech.backend.security.dto.JwtDto;
import jsaldana.tech.backend.security.dto.Login;
import jsaldana.tech.backend.security.dto.NewUser;
import jsaldana.tech.backend.security.jwt.JwtProvider;
import jsaldana.tech.backend.security.model.Role;
import jsaldana.tech.backend.security.model.User;
import jsaldana.tech.backend.security.service.RoleService;
import jsaldana.tech.backend.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class UserResource {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserService userService;
    @Autowired
    RoleService roleService;
    @Autowired
    JwtProvider provider;

    @GetMapping("/getRoles")
    public ResponseEntity<List<Role>> getRoles () {
        List<Role> roles = roleService.findRoles();
        return new ResponseEntity<>(roles, HttpStatus.OK);
    }

    @GetMapping("/getUsers")
    public ResponseEntity<List<User>> getUsers () {
        List<User> users = userService.findUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<String> newUser(@RequestBody NewUser newUser, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity<>("Errors: "+bindingResult.hasErrors(), HttpStatus.BAD_REQUEST);
        if(userService.existsByUserName(newUser.getUserName()))
            return new ResponseEntity<>("User exist by username: "+newUser.getUserName(), HttpStatus.BAD_REQUEST);
        if(userService.existsByEmail(newUser.getEmail()))
            return new ResponseEntity<>("User exist by email: "+newUser.getEmail(), HttpStatus.BAD_REQUEST);

        User user = new User(
                newUser.getName(), newUser.getLastName(), newUser.getUserName(), newUser.getEmail(),
                passwordEncoder.encode(newUser.getPassword()), newUser.getPhone(), newUser.getImageUrl(), UUID.randomUUID().toString()
        );

        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getByRoleName(RoleEnum.ROLE_USER).get());
        if(newUser.getRoles().contains("admin"))
            roles.add(roleService.getByRoleName(RoleEnum.ROLE_ADMIN).get());
        user.setRole(roles);
        userService.save(user);
        return new ResponseEntity<>("User created.",HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@RequestBody Login login, BindingResult bindingResult){
        if(bindingResult.hasErrors())
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Authentication authentication =
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login.getUserName(), login.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = provider.generateToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), userDetails.getAuthorities());
        return new ResponseEntity<>(jwtDto, HttpStatus.OK);
    }
}