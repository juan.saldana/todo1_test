package jsaldana.tech.backend.security.model;

import jsaldana.tech.backend.security.RoleEnum;

import javax.persistence.*;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Enumerated(EnumType.STRING)
    private RoleEnum roleName;

    public Role() {

    }

    public Role(RoleEnum roleName) {
        this.roleName = roleName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RoleEnum getRoleName() {
        return roleName;
    }

    public void setRolNombre(RoleEnum roleName) {
        this.roleName = roleName;
    }
}