package jsaldana.tech.backend.security.util;

import jsaldana.tech.backend.security.RoleEnum;
import jsaldana.tech.backend.security.model.Role;
import jsaldana.tech.backend.security.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

// Se usa esta clase para crear los roles por primera vez.
@Component
public class CreateRoles implements CommandLineRunner {

    @Autowired
    RoleService roleService;

    @Override
    public void run(String... args) throws Exception {
        /*Role rolAdmin = new Role(RoleEnum.ROLE_ADMIN);
        Role rolUser = new Role(RoleEnum.ROLE_USER);
        roleService.save(rolAdmin);
        roleService.save(rolUser);*/
    }
}