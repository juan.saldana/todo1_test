package jsaldana.tech.backend.security.service;

import jsaldana.tech.backend.security.RoleEnum;
import jsaldana.tech.backend.security.model.Role;
import jsaldana.tech.backend.security.repo.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RoleService {
    @Autowired
    RoleRepo roleRepo;

    public List<Role> findRoles(){
        return roleRepo.findAll();
    }

    public Optional<Role> getByRoleName(RoleEnum roleEnum){
        return roleRepo.findByRoleName(roleEnum);
    }

    public void save(Role role) {
        roleRepo.save(role);
    }
}
