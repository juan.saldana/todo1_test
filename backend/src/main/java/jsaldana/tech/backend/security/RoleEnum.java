package jsaldana.tech.backend.security;

public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER
}
