package jsaldana.tech.backend.security.service;

import jsaldana.tech.backend.security.model.User;
import jsaldana.tech.backend.security.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserService {
    @Autowired
    UserRepo userRepo;

    public List<User> findUsers(){
        return userRepo.findAll();
    }

    public Optional<User> findByUserName(String userName){
        return  userRepo.findByUserName(userName);
    }

    public boolean existsByUserName(String userName){
        return userRepo.existsByUserName(userName);
    }

    public boolean existsByEmail(String email){
        return userRepo.existsByEmail(email);
    }

    public void save(User user){
        userRepo.save(user);
    }
}