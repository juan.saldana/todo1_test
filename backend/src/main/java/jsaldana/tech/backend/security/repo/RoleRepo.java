package jsaldana.tech.backend.security.repo;

import jsaldana.tech.backend.security.RoleEnum;
import jsaldana.tech.backend.security.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepo extends JpaRepository<Role, Integer> {
    Optional<Role> findByRoleName(RoleEnum roleEnum);
}
